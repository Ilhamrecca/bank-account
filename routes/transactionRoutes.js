const express = require('express'); // Import express
const router = express.Router(); // Make router

const transactionController = require('../controllers/transactionController'); // Import transactionController
const transactionValidator = require('../middlewares/validators/transactionValidator'); // Import transactionValidator


router.post("/createTransaction/:id", transactionValidator.createTransaction, transactionController.createTransaction);


router.use((req, res, next) => {
    const err = new Error("Page Not Found");
    err.status = 404;
    next(err);
});


//ketika fungsi next dipanggil pakek fungsi ini,
router.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send({
        error: {
            message: err.message,
            success: false,
            code: err.status || 500,
        },
    });
});

module.exports = router; // Export router
