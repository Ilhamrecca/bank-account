const express = require('express'); // Import express
const router = express.Router(); // Make router

const accountController = require('../controllers/accountController'); // Import accountController
const accountValidator = require('../middlewares/validators/accountValidator'); // Import accountValidator

router.get("/getAllAccount", accountController.getAllAccount);
router.get("/getAccountDetails/:id", accountController.getAccountDetails);
router.post("/createAccount", accountValidator.createAccount, accountController.createAccount);


router.use((req, res, next) => {
    const err = new Error("Page Not Found");
    err.status = 404;
    next(err);
});


//ketika fungsi next dipanggil pakek fungsi ini,
router.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send({
        error: {
            message: err.message,
            success: false,
            code: err.status || 500,
        },
    });
});

module.exports = router; // Export router
