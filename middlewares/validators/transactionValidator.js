const { check, validationResult, matchedData, sanitize } = require('express-validator'); //form validation & sanitize form params


module.exports = {
    // createTransaction validator
    createTransaction: [

        check('currency', 'currency must be betwen 3-50').isLength({ min: 3, max: 50 }),
        check('transactionNarrative', 'transactionNarrative must be betwen 3-50').isLength({ min: 3, max: 50 }),
        check('debit', 'debit must be a number').isNumeric(),
        check('balance', 'currency must be a number').isNumeric(),

        (req, res, next) => {
            const errors = validationResult(req); // Collect errors from check function
            // If errors is not null, it will be return errors response
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped(),
                    success: false,
                    code: 422,
                });
            }

            // If no errors, it will go to next step
            next();
        }
    ],



};
