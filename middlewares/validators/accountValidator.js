const { param, check, validationResult, matchedData, sanitize } = require('express-validator'); //form validation & sanitize form params
const moment = require('moment');


module.exports = {
  createAccount: [
    check('accountNumber', 'Nama must be between 3-50').isLength({ min: 3, max: 50 }),
    check('currency', 'Lokasi must be betwen 3-50').isLength({ min: 3, max: 50 }),
    check('balance', 'email field must be email address').isNumeric(),
    (req, res, next) => {
      const errors = validationResult(req); // Collect errors from check function
      // If errors is not null, it will be return errors response
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
          success: false,
          code: 422,
        });
      }
      // If no errors, it will go to next step
      next();
    }
  ],
  login: [
    check('email', 'email field must be email address').normalizeEmail().isEmail(), // validator for email field
    check('password', 'password field must have 4 to 32 characters').isString().isLength({ min: 4, max: 32 }), // validator for password field
    (req, res, next) => {
      const errors = validationResult(req); // Collect errors from check function
      // If errors is not null, it will be return errors response
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
          success: false,
          code: 422,
        });
      }
      // If no errors, it will go to next step
      next();
    }
  ],
  changePassword: [
    check('oldPassword', 'password field must have 4 to 32 characters').isString().isLength({ min: 4, max: 32 }),
    check('password', 'password field must have 4 to 32 characters').isString().isLength({ min: 4, max: 32 }), // validator for password field
    check('passwordConfirmation', 'passwordConfirmation field must have the same value as the password field').exists().custom((value, { req }) => value === req.body.password), // validator for passwordConfirmation field

    (req, res, next) => {
      const errors = validationResult(req); // Collect errors from check function
      // If errors is not null, it will be return errors response
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
          success: false,
          code: 422,
        });
      }
      // If no errors, it will go to next step
      next();
    }
  ],

  updateProfile: [
    check("nama_Depan", "nama belakang must be a string and lengt must be between 3-50").custom(value => {
      if (value === undefined)
        return true;
      else if (value.length <= 3 || value.length > 50) {
        return false;
      }
      return true;
    }),
    check("nama_Belakang", "nama belakang must be a string and lengt must be between 3-50").custom(value => {
      if (value === undefined)
        return true;
      else if (value.length <= 3 || value.length > 50) {
        return false;
      }
      return true;
    }),
    check('jenis_Kelamin', 'gender must be female/male').custom(value => {

      if (value === undefined)
        return true;
      value = value.trim().toLowerCase();
      if (value !== "male" && value !== "female") {
        return false;
      }
      return true;
    }),
    check('tanggal_Lahir', 'Tanggal Lahir Harus berupa tanggal').custom(value => {
      if (value === undefined)
        return true;
      return moment(value).isValid();
    }),

    (req, res, next) => {
      const errors = validationResult(req); // Collect errors from check function
      // If errors is not null, it will be return errors response
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped(),
          success: false,
          code: 422,
        });
      }
      // If no errors, it will go to next step
      next();
    }
  ],

};


