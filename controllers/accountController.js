const { Account, Transaction } = require('../models') // Import models
const { sendError, sendResponse } = require("./errorHandler");

class accountController {

  async getAllAccount(req, res, next) {
    try {
      const result = await Account.findAll({})

      sendResponse("getAllAccount Success", 200, result, res);
    } catch (error) {
      const message = {
        message: "Something went wrong when getting All Account List",
        error: error.message
      }
      sendError(message, 501, next)
    }

  }

  async createAccount(req, res, next) {
    try {
      const data = {
        accountNumber: req.body.accountNumber,
        currency: req.body.currency,
        balance: req.body.balance,
      }
      const result = await Account.create(data)

      sendResponse("create Account success!", 200, result, res);
    } catch (error) {
      const message = {
        message: "Something went wrong when getting All Account List",
        error: error.message
      }
      sendError(message, 501, next)
    }

  }

  async getAccountDetails(req, res, next) {

    try {

      const result = await Account.findAll({
        where: { id: req.params.id },
        include: [{
          model: Transaction,
          attributes: ['valueDate', 'currency', 'debit', 'transactionNarrative'],
        }]
      })

      if (result.length === 0) {
        sendResponse("Account not found!", 404, result, res);
      }
      else
        sendResponse("get Account Detai ssuccess!", 200, result, res);
    } catch (error) {
      const message = {
        message: "Something went wrong when getting Account Details",
        error: error.message
      }
      sendError(message, 501, next)
    }

  }


}

module.exports = new accountController; // Export accountController
