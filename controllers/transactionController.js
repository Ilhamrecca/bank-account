const { Transaction, Account } = require('../models') // Import user model
const { sendError, sendResponse } = require("./errorHandler");

// ProdukController class declaration
class transactionController {
    async createTransaction(req, res, next) {
        try {

            const currentDate = Date.now()
            const data = {
                valueDate: currentDate,
                currency: req.body.currency,
                debit: req.body.debit,
                transactionNarrative: req.body.transactionNarrative,
                accountId: req.params.id
            }
            const result = await Transaction.create(data)
            //update last transaction on account
            const update = await Account.update(
                { lastTransaction: currentDate },
                {
                    where: { id: req.params.id },
                })


            sendResponse("creating transaction success!", 200, result, res);
        } catch (error) {
            const message = {
                message: "Something went wrong when creating transaction",
                error: error.message
            }
            sendError(message, 501, next)
        }

    }


}

module.exports = new transactionController; // Export ProdukController
