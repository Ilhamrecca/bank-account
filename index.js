const express = require('express'); // Import express
const bodyParser = require('body-parser'); // Import body-parser
const cors = require('cors');

const accountRoutes = require('./routes/accountRoutes'); // Import routes
const transactionRoutes = require('./routes/transactionRoutes'); // Import routes

const app = express(); // Make API
app.use(cors());

//Set body parser for HTTP post operation
app.use(cors());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies


app.use('/Account', accountRoutes); // If access localhost:3000, it will be go to AccountRoutes

app.use('/Transaction', transactionRoutes); // If access localhost:3000, it will be go to transactionRoutes

// Server running on port 3000
app.listen(3000, () => {
  console.log('Server running on port 3000!');
})
