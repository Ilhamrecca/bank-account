'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Transaction.belongsTo(models.Account, { foreignKey: 'accountId' })
    }
  };
  Transaction.init({
    valueDate: {
      type: DataTypes.DATE,
      require: true
    },
    credit: {
      type: DataTypes.INTEGER,
      require: true
    },
    currency: {
      type: DataTypes.STRING,
      require: true
    },
    debit: {
      type: DataTypes.FLOAT,
      require: true
    },
    transactionNarrative: {
      type: DataTypes.STRING,
      require: true
    },
  }, {
    sequelize,
    modelName: 'Transaction',
  });
  return Transaction;
};