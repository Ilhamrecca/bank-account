'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Account.hasMany(models.Transaksi, { as: 'transaksi' })
      Account.hasMany(models.Transaction)
    }
  };
  Account.init({
    accountNumber: {
      require: true,
      type: DataTypes.INTEGER
    },
    currency: {
      type: DataTypes.INTEGER
    },
    balance: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    lastTransaction: {
      type: DataTypes.DATE,
    },
  }, {
    sequelize,
    modelName: 'Account',
  });
  return Account;
};